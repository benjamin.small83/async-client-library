from async_client_library.factories.base import BaseFactory
from async_client_library.models.base import BaseModel
from async_client_library.exceptions import BaseException

class PokeException(BaseException):
    pass

class PokeApi(BaseFactory):

    base_url = "https://pokeapi.co/api/v2"

class Pokemon(BaseModel):
    pass

class PokemonFactory(PokeApi):

    path = "/pokemon"

    async def get(self, name=None, pokemon_id=None):

        if name is None and id is None:
            raise PokeException("Pokemon.get requires either a name or pokemon_id to be passed")

        path = f"{self.path}/{name or pokemon_id}"

        resp = await self.api_get_request(path)

        return Pokemon(**resp)

    async def list(self, page_size=200):

        path = f"{self.path}/"

        init_resp = await self.api_get_request(path=path, params={"limit": page_size})

        total_count = init_resp['count']

        for res in init_resp['results']:
            yield Pokemon(**res)

        for page_number in range(1, int(total_count / page_size) + 1):
            offset = page_number * page_size
            resp = await self.api_get_request(path=path, params={"limit": page_size, "offset": offset})
            for res in resp['results']:
                yield Pokemon(**res)



if __name__ == "__main__":

    from pprint import pprint
    import asyncio
    import aiohttp

    # async def _coro():
    #     async with aiohttp.ClientSession() as session:
    #
    #         pf = PokemonFactory(session=session, verify_ssl=False)
    #
    #         return await asyncio.gather(
    #             pf.get("pikachu"),
    #             pf.get("squirtle"),
    #         )
    #
    # data = asyncio.run(_coro())
    # data

    async def _coro():
        poke_list = []
        async for pokemon in PokemonFactory().list():
            poke_list.append(pokemon)
        return poke_list

    data = asyncio.run(_coro())
    data

