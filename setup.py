#! /usr/bin/env python3
from setuptools import setup, find_packages
from distutils.util import convert_path

main_ns = {}
ver_path = convert_path('async_client_library/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(

    name             = "async-client-library",
    packages         = find_packages(exclude=["tests"]),
    version          = main_ns['__version__'],
    install_requires = ["aiohttp"],
    description      = 'Async Client Library',
    author           = 'Ben Small',
    author_email     = 'benjamin.small83@gmail.com',
    url              = '',
    python_requires  = ">=3.6"
)
